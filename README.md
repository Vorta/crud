# CRUD task example setup instructions

The site example was build on PhalconPHP framework which exists as a PHP extension. To set it up, please
direct your Apache or Nginx to the public folder where index.php is situated.

## Database info (MySQL):

host:		'localhost',
username:	crud_db_user
password:	r9E6Y?Zx9H5}BCW.KmBa>fxSjJN7NhXGM(t)mQ4Q
dbname:		crud_db


## PHP Info:

Minimum PHP version: 7.1
Required extension:  [PhalconPHP](https://phalconphp.com/en/download)


## Nginx config (if required):

```
	server {

		listen			127.0.0.1:80;
		server_name		~^www\.crud\.loc$;  # Pick any domain

		index index.php index.html index.htm;

		set $root_path		"your-path-to-the-project/public";
		root				$root_path;

		charset utf-8;
		autoindex off;

		access_log	"your-path-to-the_access.log";
		error_log	"your-path-to-the_error.log";

		location @rewrite {
			rewrite ^/(.*)$ /index.php?_url=/$1;
		}

		try_files $uri $uri/ @rewrite;

		location ~ \.php$ {
			fastcgi_split_path_info ^(.+\.php)(.*)$;
			include		nginx.fastcgi.conf;
			fastcgi_pass    php_farm;
			fastcgi_read_timeout 300; 
		}

	}
```
