CREATE DATABASE IF NOT EXISTS `crud_db` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci */;
USE `crud_db`;


CREATE TABLE `country` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`code` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL,
	`name` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


CREATE TABLE `user` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(64) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
	`surname` varchar(64) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
	`email` varchar(256) COLLATE utf8mb4_unicode_520_ci NOT NULL,
	`username` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
	`password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
	`birthdate` date NOT NULL,
	`gender` char(1) COLLATE utf8mb4_unicode_520_ci NOT NULL,
	`country_id` int(11) unsigned DEFAULT NULL,
	`deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`),
	KEY `user_country_fk_idx` (`country_id`),
	CONSTRAINT `user_country_fk` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
