<?php

// Environments
define('DEVELOPMENT', 1);
define('PRODUCTION', 2);

define('ENVIRONMENT', DEVELOPMENT);

// Get the server's timezone, we'll need this to ensure it's in sync with MySQL DB
date_default_timezone_set('Europe/Zagreb');
define('TIMEZONE', (new \DateTime())->format('P'));

// Set doctype
\Phalcon\Tag::setDoctype(\Phalcon\Tag::HTML5);
