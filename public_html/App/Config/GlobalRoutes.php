<?php

class GlobalRoutes extends \Phalcon\Mvc\Router\Group {

	/**
	 * The most generic routing, but it is sufficient for the task
	 * 
	 * @author           Domagoj Polović (Vorta)
	 * @version          1.0
	 * @since            1.0
	 * @package          CRUD
	 */
	public function initialize () {

		$this->add('/:controller/:action/:params', [
			'controller'	=>	1,
			'action'		=>	2,
			'params'		=>	3
		]);

	}

}
