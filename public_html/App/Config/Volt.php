<?php

/**
 * Setting up the volt engine
 * 
 * @author           Domagoj Polović (Vorta)
 * @version          1.0
 * @since            1.0
 * @package          CRUD
 */
return function($view, $di) {

	$volt = new \Phalcon\Mvc\View\Engine\Volt($view, $di);

	$pathToCompiled = '../Var/Volt/';

	if (!file_exists($pathToCompiled)) {
		mkdir($pathToCompiled, 0777, TRUE);
	}

	$volt->setOptions([
		'compiledPath' => $pathToCompiled,
		'compiledSeparator' => '_',
		'compileAlways' => (ENVIRONMENT == DEVELOPMENT)
	]);

	$compiler = $volt->getCompiler();

	$functions = [
		'printR'		=> function ($resolvedArgs, $exprArgs) { return '"<pre>". print_r(' . $resolvedArgs . '->toArray(), true) ."</pre>"'; },
		'echoIfSet'		=> function ($resolvedArgs, $exprArgs) { return 'isset('. $resolvedArgs .') ? '. $resolvedArgs .' : "" '; },
		'getClass'		=> 'get_class',
		'pregMatch'		=> 'preg_match'
	];

	foreach ($functions as $key => $value) $compiler->addFunction($key, $value);

	return $volt;

};
