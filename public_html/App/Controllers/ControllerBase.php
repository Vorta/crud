<?php

class ControllerBase extends \Phalcon\Mvc\Controller {

	/**
	 * Base controller prepares the always-used CSS and JS files
	 * 
	 * @author           Domagoj Polović (Vorta)
	 * @version          1.0
	 * @since            1.0
	 * @package          CRUD
	 */
	public function initialize () {

		$this->tag->setTitle('CRUD Example');
		$this->tag->setTitleSeparator(' | ');

		$this->assets->collection('style')
		             ->addCss('assets/css/bootstrap.min.css', TRUE, FALSE);

		$this->assets->collection('head_js')
		             ->addJs('assets/third-party/js/jquery.min.js', TRUE, FALSE)
		             ->addJs('assets/third-party/js/bootstrap.min.js', TRUE, FALSE);

		$this->assets->collection('ie')
		             ->addJs('assets/js/html5shiv.min.js', TRUE, FALSE);

		$this->assets->collection('foot_js');

	}

}
