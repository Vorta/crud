<?php

use CRUD\Forms\RegistrationForm;
use CRUD\Models\User;


class IndexController extends ControllerBase {

	/**
	 * Initialization of the controller
	 * 
	 * @author           Domagoj Polović (Vorta)
	 * @version          1.0
	 * @since            1.0
	 * @package          CRUD
	 */
	public function initialize () {

		parent::initialize();

		$this->tag->appendTitle('Registration form');

	}

	/**
	 * Show the home page and its form, in case of POST, process the data validation and user creation
	 * 
	 * @author           Domagoj Polović (Vorta)
	 * @version          1.0
	 * @since            1.0
	 * @package          CRUD
	 */
	public function indexAction () {

		// Initializing the form object
		$registrationForm = new RegistrationForm();

		// If there is a POST, go into data-processing.
		// I'm using do-while to easily break out of the process if there is validation failure
		if ($this->request->isPost()) do {

			// Creating the new User object to be able to bind data to it
			$user = new User();

			// Binding the posted data to User object and the form
			$registrationForm->bind($this->request->getPost(), $user);

			// Processing the form's validation
			if (!$registrationForm->isValid()) {

				foreach ($registrationForm->getMessages() as $message) {
					$this->flash->error($message);
				}
				break;

			}

			// Converting the 3 birthday fields data  to a single DateTime object
			$user->birthdate = new \DateTime($this->request->getPost('dobY') ."-". $this->request->getPost('dobM') ."-". $this->request->getPost('dobD'));

			// Attempting to create the user, this triggers the model's validator
			if (!$user->save()) {

				foreach ($user->getMessages() as $message) {
					$this->flash->error($message);
				}
				break;

			}

			// If we got to here, there were no validation failures
			$this->flash->success("User added successfully! Please check the database.");
			$this->view->hideForm = TRUE;

		} while (FALSE);

		// Pasing the form object to the view
		$this->view->form = $registrationForm;

	}

}

