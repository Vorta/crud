<?php

namespace CRUD\Forms;

use Phalcon\Forms\Element\Email;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use CRUD\Forms\Element\DateOfBirth;

use Phalcon\Validation\Validator;
use CRUD\Validation\Validator\DateValidator;

use CRUD\Models\Country;

class RegistrationForm extends \Phalcon\Forms\Form {

	/**
	 * Registration form setup
	 * 
	 * @author           Domagoj Polović (Vorta)
	 * @version          1.0
	 * @since            1.0
	 * @package          CRUD
	 */
	public function initialize () {

		// User's first name
		$name = new Text("name");
		$name->setLabel("Name");
		$name->addValidators([
			new Validator\PresenceOf([
				'message' => "Name is mandatory!"
			])
		]);

		$this->add($name);


		// User's surname
		$surname = new Text("surname");
		$surname->setLabel("Surname");
		$surname->addValidators([
			new Validator\PresenceOf([
				'message' => "Surname is mandatory!"
			])
		]);

		$this->add($surname);


		// Email address
		$email = new Email('email');
		$email->setLabel('Email address:');
		$email->addValidators([
			new Validator\PresenceOf([
				'message' => "You forgot to insert your email address."
			]),
			new Validator\Email([
				'message' => "Please input a valid email address."
			])
		]);

		$this->add($email);


		// Username (used for login)
		$username = new Text("username");
		$username->setLabel("Username");
		$username->addValidators([
			new Validator\PresenceOf([
				'message' => "Username is mandatory!",
				'cancelOnFail' => TRUE
			]),
			new Validator\StringLength([
	 			'min' => 5,
				'messageMinimum' => 'Username is too short. It should have at least 5 characters.',
			]),
			new Validator\Alnum([
				'message' => "Username should consist only of alphanumeric characters!"
			])
		]);

		$this->add($username);


		// New password (used for login)
		$newPassword = new Password('password', [
			'placeholder' => '6 to 32 characters, please'
		]);
		$newPassword->setLabel('Password:');
		$newPassword->addValidators([
			new Validator\PresenceOf([
				'message' => 'The new password field must not be empty.',
				'cancelOnFail' => TRUE
			]),
			new Validator\StringLength([
	 			'min' => 6,
	 			'max' => 32,
				'messageMinimum' => 'Please make sure your password is between 6 and 32 characters long.',
			])
		]);
		$newPassword->setDefault('');

		$this->add($newPassword);


		// Confirm Password
		$newPasswordConfirm = new Password('passwordRepeat', [
			'placeholder' => 'Re-type your password'
		]);

		$newPasswordConfirm->setLabel('Confirm password:');
		$newPasswordConfirm->addValidators([
			new Validator\PresenceOf([
				'message' => 'The new password confirmation field must not be empty.',
				'cancelOnFail' => TRUE
			]),
			new Validator\Confirmation([
				'message' => 'Passwords do not match!',
				'with' => 'password'
			])
		]);
		$newPasswordConfirm->setDefault('');

		$this->add($newPasswordConfirm);


		// Date of birth
		$dobKey = 'dob';
		$dob = new DateOfBirth($dobKey);
		$dob->setLabel("Date of birth");
		$dob->addValidators([
			new DateValidator([
				'message' => 'Please input a valid date!',
				'cancelOnFail' => TRUE
			])
		]);
		$dob->setDefault([
			'D' => $this->request->getPost($dobKey.'D'),
			'M' => $this->request->getPost($dobKey.'M'),
			'Y' => $this->request->getPost($dobKey.'Y')
		]);

		$this->add($dob);


		// Gender select
		$gender = new Select('gender', [
			"M" => "Male",
			"F" => "Female"
		], [
			'useEmpty' => TRUE,
			'emptyText' => 'Please select...',
			'emptyValue' => '@'
		]);
		$gender->setLabel('Gender:');
		$gender->addValidators([
			new Validator\InclusionIn([
				"message" => "Please select your gender.",
				"domain"  => ["F", "M"]
			])
		]);

		$this->add($gender);


		// Country select
		$country = new Select('countryId', Country::find());
		$country->setLabel("Country");
		$country->setAttributes([
			'using' => ['id', 'name'],
			'useEmpty' => TRUE,
			'emptyText' => 'Please select...',
			'emptyValue' => '@'
		]);
		$country->addValidators([
			new Validator\ExclusionIn([
				"message" => "Please select your country.",
				"domain"  => ["@"]
			])
		]);

		$this->add($country);
	}

}
