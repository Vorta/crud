<?php

namespace CRUD\Models;

class Country extends \Phalcon\Mvc\Model {

	/**
	 * @var integer
	 */
	public $id;

	/**
	 * @var string (2)
	 */
	public $code;

	/**
	 * @var string
	 */
	public $name;


	/**
	 * Model that handles Country table in the database
	 * 
	 * @author           Domagoj Polović (Vorta)
	 * @version          1.0
	 * @since            1.0
	 * @package          CRUD
	 */
	public function initialize() {

		$this->setSource('country');

		$this->hasMany('id', 'CRUD\Models\User', 'countryId');

	}

	/**
	 * Independent Column Mapping
	 *
	 * @author           Domagoj Polović (Vorta)
	 * @version          1.0
	 * @since            1.0
	 * @package          CRUD
	 * @return           array
	 */
	public function columnMap() {
		return [
			'id' => 'id',
			'code' => 'code',
			'name' => 'name'
		];
	}

}
