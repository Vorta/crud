<?php

namespace CRUD\Models;

use Phalcon\Mvc\Model\Behavior\SoftDelete;
use Phalcon\Validation;
use Phalcon\Validation\Validator;

class User extends \Phalcon\Mvc\Model {

	/**
	 * @var integer
	 */
	public $id;

	/**
	 * @var string
	 */
	public $name;

	/**
	 * @var string
	 */
	public $surname;

	/**
	 * @var string
	 */
	public $email;

	/**
	 * @var string
	 */
	public $username;

	/**
	 * @var string
	 */
	public $password;

	/**
	 * @var date
	 */
	public $birthdate;

	/**
	 * @var char(1)
	 */
	public $gender;

	/**
	 * @var int
	 */
	public $countryId;

	/**
	 * @var boolean
	 */
	public $deleted;


	/**
	 * Model that handles User table in the database
	 * 
	 * @author           Domagoj Polović (Vorta)
	 * @version          1.0
	 * @since            1.0
	 * @package          CRUD
	 */
	public function initialize () {

		$this->setSource('user');

		$this->belongsTo('countryId', 'CRUD\Models\Country', 'id');

		$this->addBehavior(new SoftDelete ([
			'field' => 'deleted',
			'value' => 1
		]));

	}


	/**
	 * Setup the model-level validation
	 *
	 * @author           Domagoj Polović (Vorta)
	 * @version          1.0
	 * @since            1.0
	 * @package          CRUD
	 * @return           array
	 */
	public function validation () {

		$validator = new Validation();

		$validator->add('username', new Validator\Uniqueness([
			'model' => $this,
			'message' => 'Username already taken!'
		]));

		$validator->add('email', new Validator\Email([
			'required' => TRUE,
			'message' => 'Valid e-mail address is required!'
		]));

		$validator->add('email', new Validator\Uniqueness([
			'model' => $this,
			'message' => 'This e-mail address is already taken!'
		]));

		$validator->add('password', new Validator\StringLength([
			'field' => 'password',
			'max' => 32,
			'min' => 6,
			'messageMaximum' => "Your password must be under 32 characters",
			'messageMinimum' => "Your password must be at least 6 characters"
		]));

		return $this->validate($validator);
		
	}


	/**
	 * Independent Column Mapping
	 *
	 * @author           Domagoj Polović (Vorta)
	 * @version          1.0
	 * @since            1.0
	 * @package          CRUD
	 * @return           array
	 */
	public function columnMap () {
		return [
			'id' => 'id', 
			'name' => 'name', 
			'surname' => 'surname', 
			'email' => 'email',
			'username' => 'username',
			'session_id' => 'sessionID',
			'password' => 'password',
			'birthdate' => 'birthdate', 
			'gender' => 'gender', 
			'country_id' => 'countryId', 
			'creation_date' => 'creationDate', 
			'deleted' => 'deleted'
		];
	}


	/**
	 * Hash the user's password before creating the DB entry
	 *
	 * @author           Domagoj Polović (Vorta)
	 * @version          1.0
	 * @since            1.0
	 * @package          CRUD
	 * @return           array
	 */
	public function afterValidationOnCreate () {
		$this->password = $this->getDi()->getShared('security')->hash($this->password);
	}


	/**
	 * Handle data conversion before saving to DB
	 *
	 * @author           Domagoj Polović (Vorta)
	 * @version          1.0
	 * @since            1.0
	 * @package          CRUD
	 * @return           array
	 */
	public function beforeSave () {

		$this->birthdate = is_null($this->birthdate) ? NULL : $this->birthdate->format('Y-m-d');
	
	}


	/**
	 * Handle data conversion after fetching from DB
	 *
	 * @author           Domagoj Polović (Vorta)
	 * @version          1.0
	 * @since            1.0
	 * @package          CRUD
	 * @return           array
	 */
	public function afterFetch () {

		$this->birthdate = is_null($this->birthdate) ? NULL : new \DateTime($this->birthdate);

	}


	/**
	 * Restore data conversion after saving to DB
	 *
	 * @author           Domagoj Polović (Vorta)
	 * @version          1.0
	 * @since            1.0
	 * @package          CRUD
	 * @return           array
	 */
	public function afterSave () {

		$this->birthdate = is_null($this->birthdate) ? NULL : new \DateTime($this->birthdate);

	}

}