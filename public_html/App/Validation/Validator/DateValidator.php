<?php

namespace CRUD\Validation\Validator;

class DateValidator extends \Phalcon\Validation\Validator implements \Phalcon\Validation\ValidatorInterface {

	/**
	 * Adding a date validator for the custom DateOfBirth field
	 * 
	 * @author           Domagoj Polović (Vorta)
	 * @version          1.0
	 * @since            1.0
	 * @package          CRUD
	 */
	public function validate (\Phalcon\Validation $validator, $attribute) {

		$d = $validator->getValue($attribute.'D');
		$m = $validator->getValue($attribute.'M');
		$y = $validator->getValue($attribute.'Y');

		if (!is_numeric($d) || !is_numeric($m) || !is_numeric($y) || !checkdate((int) $m, (int) $d, (int) $y))
		{
			$message = $this->getOption('message');
			if (!$message)
			{
				$message = "The date you input is not valid.";
			}

			$validator->appendMessage(new Message($message, $attribute, 'Date of birth'));

			return FALSE;
		}

		return TRUE;

	}
	
}
