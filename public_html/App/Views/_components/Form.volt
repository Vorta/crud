{{ flash.output() }}

{% if hideForm is not defined %}
<form method="POST" enctype="multipart/form-data" action="{{form.getAction()}}">
	{% for element in form %}
		{% include "_components/FormElement.volt" %}
	{% endfor %}

	<button class="btn btn-primary" type="submit">Create user</button>
</form>
{% endif %}