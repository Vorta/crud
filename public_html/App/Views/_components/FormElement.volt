<div class="form-group">
	<label for="{{element.getName()}}">
		{{element.getLabel()}}
	</label>
	{{element.render(['class': 'form-control'])}}
</div>
