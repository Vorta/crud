{{get_doctype()}}
<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		{{assets.outputCss('style')}}

		{{assets.outputJs('head_js')}}

		<!--[if lt IE 9]>
		{{ assets.outputJs('ie') }}
		<![endif]-->

		{{ get_title() }}

	</head>

	<body>

		<main class="container">
			{% block main %}
			{% endblock %}
		</main>

		{{assets.outputJs('foot_js')}}
	</body>

</html>