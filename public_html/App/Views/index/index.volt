{% extends "_templates/baseTemplate.volt" %}

{% block main %}

	<div class="row">
		<div class="col">
			<h1>User registration</h1>
			<p>Enter your data and click &quot;Create user&quot;. The data will be validated and if all is good, user will be created in the database.</p>
			
			{% include "_components/Form.volt" %}
			
		</div>
	</div>

{% endblock %}