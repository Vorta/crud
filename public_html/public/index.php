<?php


/*
 *---------------------------------------------------------------
 * APPLICATION CONSTANTS
 *---------------------------------------------------------------
 *
 * Setup app-wide constants
 *
 */
define('APPROOT', dirname(__DIR__) ."/App");

require APPROOT .'/Config/Constants.php';


/*
 *---------------------------------------------------------------
 * DEBUGGING ENVIRONMENT FOR DEVELOPMENT
 *---------------------------------------------------------------
 *
 * Setting up debugging mode if environment is dev
 *
 */

if (ENVIRONMENT == DEVELOPMENT) {
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(-1);

	$debug = new \Phalcon\Debug();
	$debug->listen();
}

/*
 *---------------------------------------------------------------
 * APPLICATION
 *---------------------------------------------------------------
 *
 * Let's start everything up
 *
 */

try {
	// Autoloader
	$loader = new \Phalcon\Loader();

	$loader->registerDirs([
		APPROOT .'/Controllers/',
		APPROOT .'/Models/',
		APPROOT .'/Config/'
	]);

	$loader->registerNamespaces([
		'CRUD' =>	APPROOT
	]);

	$loader->register();

	// Dependancy injection
	$di = new \Phalcon\DI\FactoryDefault();

	// Database
	$di->set('db', function () {
		$db = new \Phalcon\Db\Adapter\Pdo\Mysql([
			'host' => 'localhost',
			'username' => 'crud_db_user',
			'password' => 'r9E6Y?Zx9H5}BCW.KmBa>fxSjJN7NhXGM(t)mQ4Q',
			'dbname' => 'crud_db',
			'charset'  => 'utf8'
		]);
		$db->execute("SET time_zone='". TIMEZONE ."';");
		return $db;
	});

	// View
	$di->set('view', function() {

		$view = new \Phalcon\Mvc\View();
		$view->setViewsDir(APPROOT .'/Views/');

		Phalcon\Tag::setDoctype(\Phalcon\Tag::HTML5);

		$view->registerEngines([
			'.volt' => include APPROOT .'/Config/Volt.php'
		]);

		return $view;

	});

	$di->set('url', function() {
		$url = new Phalcon\Mvc\Url();
		$url->setBaseUri("/");
		return $url;
	});

	$di->set('security', function() {
		$security = new Phalcon\Security();
		$security->setWorkFactor(12);
		return $security;
	});

	$di->set('router', function () {
		$router = new \Phalcon\Mvc\Router();
		$router->mount(new GlobalRoutes());

		$router->setDefaultController('index');
		$router->setDefaultAction('index');

		return $router;
	});

	// Session
	$di->setShared('session', function () {
		$session = new \Phalcon\Session\Adapter\Files();
		$session->start();
		return $session;
	});

	// Flashdata
	$di->set('flash', function () {
		$flash = new \Phalcon\Flash\Session([
			'error' => 'alert alert-danger',
			'success' => 'alert alert-success',
			'notice' => 'alert alert-info',
			'warning' => 'alert alert-warning'
		]);
		return $flash;
	});

	// App deployment
	$app = new \Phalcon\Mvc\Application($di);

	echo $app->handle()->getContent();

} catch (\Phalcon\Exception $e) {
	echo $e->getMessage();
	echo '<pre>';
	echo get_class($e), ": ", $e->getMessage(), "\n";
	echo " File=", $e->getFile(), "\n";
	echo " Line=", $e->getLine(), "\n";
	echo $e->getTraceAsString();
}
